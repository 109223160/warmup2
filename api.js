	$("#login").submit(function(e) {	
		$.ajax({
			url: '/login',
			dataType: 'json',
			type: 'post',
			data: JSON.stringify({ username: $('#username').val(), password: $('#password').val() }),
			contentType: "application/json",
			complete: callback
		});
	});


	$("#adduser").submit(function(e) {
			$.ajax({
                	url: '/adduser',
                	dataType: 'json',
                	type: 'post',
                	data: JSON.stringify({ username: $('#username').val(), password: $('#password').val(), email: $('#email').val() }),
                	contentType: "application/json",
                	complete: callback
        	});
	});


	$("#logout").submit(function(e) {
		$.ajax({
                	url: '/logout',
                	dataType: 'json',
                	type: 'post',
                	data: JSON.stringify({ status: "OK" }),
                	contentType: "application/json",
                	complete: callback,
			success: function(data) {
				window.location = 'logout.php';
			}
        	});
	});


 	$("#listconv").submit(function(e) {
		$.ajax({
			url: '/listconv',
			dataType: 'json',
			type: 'post',
			data: JSON.stringify({ status: "OK", conversations: [  ] }),
			contentType: "application/json",	
			complete: callback
		});
	});


	$("#getconv").submit(function(e) {
		$.ajax({
                	url: '/getconv',
                	dataType: 'json',
                	type: 'post',
                	data: JSON.stringify({ status: "OK", conversation: [ {timestamp: $, name: $,  }]}),
                	contentType: "application/json",
                	complete: callback
        	});
	});
	
