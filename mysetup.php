<?php
$servername = "localhost";
$username = "JRAL";
$password = "jral";
$dbname = "ELIZA";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

$sql = "CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` tinyint(1) DEFAULT 0 NOT NULL,
  `hash` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`, `email`)
)";

if ($conn->query($sql) == TRUE) {
	echo "Table user created successfully";
}
else {
	echo "Error creating table: " . $conn->error;
}

# WE WILL HAVE A TABLE FOR CONVERSATIONS THAT WILL HOLD /listconv stuff
# conv_id = unique id for /getconv 
# start_date for /listconv
# id is foreign key from user table to identify their conversations
# all other is explained from json data we want

$sql_convos = "CREATE TABLE IF NOT EXISTS `conversations` (
 	`convo_id` int(11) NOT NULL AUTO_INCREMENT,
	`start_date` DATE,
	`id` int(11),
	`username` varchar(255), 
	PRIMARY KEY (`convo_id`),
	FOREIGN KEY (`username`) REFERENCES user(`username`),
	FOREIGN KEY (`id`) REFERENCES user(`id`)
)";

if ($conn->query($sql_convos) == TRUE) {
	echo "Table conversation created successfully";
}
else {
	echo "Error creating table: " . $conn->error;
}

# name is username or eliza
# convo id is a foreign key; will be used to identify a cluster of a conversation if same id


$sql_single_convo = "CREATE TABLE IF NOT EXISTS `single_convo` (
	`convo_id` int(11),
	`counter` int(11) NOT NULL AUTO_INCREMENT,
	`timestamp` TIME,
        `name` varchar(255) NOT NULL,
        `text` varchar(255) NOT NULL,
	PRIMARY KEY (`counter`),
	FOREIGN KEY (`convo_id`) REFERENCES conversations(`convo_id`)
)";

if ($conn->query($sql_single_convo) == TRUE) {
        echo "Table single conversation created successfully";
}
else {
        echo "Error creating table: " . $conn->error;
}



$conn->close();
>
